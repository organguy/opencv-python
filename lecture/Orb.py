import cv2

def ORB():
    img = cv2.imread('../images/model.jpeg')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img2 = None

    orb = cv2.ORB_create()
    kp, des = orb.detectAndCompute(img, None)

    img2 = cv2.drawKeypoints(img, kp, img2, (0, 0, 255), flags=0)

    cv2.imshow('ORB', img2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


ORB()