import numpy as np
import cv2
import matplotlib.pyplot as pit

imgFile = '../images/museum.png'
img = cv2.imread(imgFile)


def showImage(image):
    pit.imshow(image, cmap='Accent', interpolation='bicubic')
    pit.xticks([])
    pit.yticks([])
    pit.title('museum')
    pit.show()


def cropImage():
    subimg = img[300:400, 350:750]
    showImage(subimg)
    return subimg


def pasteImage(sub):
    img[300:400, 0:400] = sub
    showImage(img)


def splitColorChannel():
    b, g, r = cv2.split(img)

    showImage(b)
    showImage(g)
    showImage(r)


def mergedColorChannel():
    b, g, r = cv2.split(img)
    mergedIamge = cv2.merge((b, g, r))
    showImage(mergedIamge)


# showImage(img)

# subimg = cropImage()

# pasteImage(subimg)

# splitColorChannel()

mergedColorChannel()
