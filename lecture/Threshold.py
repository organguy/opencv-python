import cv2
import matplotlib.pyplot as plt

img = cv2.imread('../images/model.jpeg', cv2.IMREAD_GRAYSCALE)


def showThreshold(flag):
    ret, thr = cv2.threshold(img, 127, 255, flag)
    cv2.imshow('threshold', thr)


def showAdaptiveThreshold(method, type):
    thr = cv2.adaptiveThreshold(img, 255, method, type, 11, 2)
    cv2.imshow('adaptive threshold', thr)


def showOtsuThreshold():
    ret, thr1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

    ret, thr2 = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    blur = cv2.GaussianBlur(img, (5, 5), 0)
    ret, thr3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    titles = ['original noisy', 'Histogram', 'G-Thresholding',
              'original noisy', 'Histogram', 'Otsu Thresholding',
              'Gaussian-filtered', 'Histogram', 'Otsu Thresholding']

    images = [img, 0, thr1, img, 0, thr2, blur, 0, thr3]

    for i in range(3):
        plt.subplot(3, 3, i * 3 + 1), plt.imshow(images[i * 3], 'gray')
        plt.title(titles[i * 3]), plt.xticks([]), plt.yticks([])

        plt.subplot(3, 3, i * 3 + 2), plt.hist(images[i * 3].ravel(), 256)
        plt.title(titles[i * 3]), plt.xticks([]), plt.yticks([])

        plt.subplot(3, 3, i * 3 + 3), plt.imshow(images[i * 3 + 2], 'gray')
        plt.title(titles[i * 3 + 2]), plt.xticks([]), plt.yticks([])

    plt.show()


def showImage(image):
    plt.imshow(image)
    plt.xticks([])
    plt.yticks([])
    plt.title('museum')
    plt.show()


# showImage(img)
# showThreshold(cv2.THRESH_BINARY)
# showThreshold(cv2.THRESH_BINARY_INV)
# showThreshold(cv2.THRESH_TRUNC)
# showThreshold(cv2.THRESH_TOZERO)
# showThreshold(cv2.THRESH_TOZERO_INV)
# showAdaptiveThreshold(cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY)
# showAdaptiveThreshold(cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV)
# showAdaptiveThreshold(cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY)
# showAdaptiveThreshold(cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV)
showOtsuThreshold()

cv2.waitKey(0)
cv2.destroyAllWindows()
