import cv2
import matplotlib.pyplot as pit


def showImage():
    imgFile = '../images/no_sound.png'
    img = cv2.imread(imgFile, cv2.IMREAD_COLOR)

    cv2.imshow('model', img)
    k = cv2.waitKey(0)

    if k == 27:
        cv2.destroyAllWindows()
    elif k == ord('c'):
        writeImage(img)


def writeImage(img):
    copyFile = '../images/copy_sound.png'
    cv2.imwrite(copyFile, img)
    cv2.destroyAllWindows()


def showimageByPit():
    imgFile = '../images/no_sound.png'
    img = cv2.imread(imgFile, cv2.IMREAD_GRAYSCALE)

    pit.imshow(img, cmap='Accent', interpolation='bicubic')
    pit.xticks([])
    pit.yticks([])
    pit.title('model')
    pit.show()


def getImagePixel(x, y):
    imgFile = '../images/no_sound.png'
    img = cv2.imread(imgFile, cv2.IMREAD_COLOR)
    px = img[x, y]
    return px


def setImagePixelBlack(x, y):
    imgFile = '../images/no_sound.png'
    img = cv2.imread(imgFile, cv2.IMREAD_COLOR)

    img.itemset((x, y, 0), 0)
    img.itemset((x, y, 1), 0)
    img.itemset((x, y, 2), 0)

    pit.imshow(img, cmap='Accent', interpolation='bicubic')
    pit.xticks([])
    pit.yticks([])
    pit.title('model')
    pit.show()


def getImageAttribute():
    imgFile = '../images/no_sound.png'
    img = cv2.imread(imgFile, cv2.IMREAD_COLOR)

    print(img.shape)
    print(img.size)
    print(img.dtype)


def showImage(image):
    pit.imshow(image)
    pit.xticks([])
    pit.yticks([])
    pit.title('museum')
    pit.show()


def addImage(imgfile1, imgfile2):
    img1 = cv2.imread(imgfile1)
    img2 = cv2.imread(imgfile2)

    # cv2.imshow('img1', img1)
    # cv2.imshow('img2', img2)

    add_img1 = img1 + img2
    add_img2 = cv2.add(img1, img2)

    # cv2.imshow('img1+img2', add_img1)
    # cv2.imshow('add(img2, img2', add_img2)

    showImage(add_img2)


def onMouse(x):
    pass


def imgBlending(imgfile1, imgfile2):
    img1 = cv2.imread(imgfile1)
    img2 = cv2.imread(imgfile2)

    cv2.namedWindow('ImgPane')
    cv2.createTrackbar('MIXING', 'ImgPane', 0, 100, onMouse)
    mix = cv2.getTrackbarPos('MIXING', 'ImgPane')

    while True:
        img = cv2.addWeighted(img1, float(100 - mix) / 100, img2, float(mix) / 100, 0)
        cv2.imshow('ImgPane', img)

        k = cv2.waitKey(1)
        if k == 27:
            break

        mix = cv2.getTrackbarPos('MIXING', 'ImgPane')

    cv2.destroyAllWindows()


def bitOperation(hpos, vpos):
    img1 = cv2.imread('../images/museum.png')
    img2 = cv2.imread('../images/opencv.jpeg')

    rows, cols, channels = img2.shape
    roi = img1[vpos:rows+vpos, hpos:cols+hpos]

    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)

    img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)

    img2_fg = cv2.bitwise_and(img2, img2, mask=mask)

    dst = cv2.add(img1_bg, img2_fg)
    img1[vpos:rows + vpos, hpos:cols + hpos] = dst

    showImage(img1)


# showImage()
# showimageByPit()
# addImage('../images/tutorial_1.png', '../images/tutorial_2.png')
# imgBlending('../images/tutorial_1.png', '../images/tutorial_2.png')
bitOperation(10, 10)
