import numpy as np
import cv2
import matplotlib.pyplot as plt


def histogram():
    img1 = cv2.imread('../images/model.jpeg', cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread('../images/model.jpeg')

    hist1 = cv2.calcHist([img1], [0], None, [256], [0, 256])

    hist2, bins = np.histogram(img1.ravel(), 256, [0, 256])

    hist3 = np.bincount(img1.ravel(), minlength=256)

    plt.hist(img1.ravel(), 256, [0, 256])

    color = ('b', 'g', 'r')

    for i, col in enumerate(color):
        hist = cv2.calcHist([img2], [i], None, [256], [0, 256])
        plt.plot(hist, color=col)
        plt.xlim([0, 256])

    plt.show()


def histogramEqualizationWithNumpy():
    img = cv2.imread('../images/histogram_equalization.png', cv2.IMREAD_GRAYSCALE)

    hist, bins = np.histogram(img.ravel(), 256, [0, 256])
    cdf = hist.cumsum()

    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
    cdf = np.ma.filled(cdf_m, 0).astype('uint8')

    img2 = cdf[img]

    cv2.imshow('Hstogram Equalization', img2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def histogramEqualization():
    img = cv2.imread('../images/histogram_equalization.png', cv2.IMREAD_GRAYSCALE)

    equ = cv2.equalizeHist(img)
    res = np.hstack((img, equ))
    cv2.imshow('equalizer', res)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


histogramEqualization()