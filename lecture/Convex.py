import cv2
import numpy as np

def convex():
    img = cv2.imread('../images/contour.jpg')
    img1 = img.copy()
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[1]
    cv2.drawContours(img, [cnt], 0, (0, 255, 0), 3)

    check = cv2.isContourConvex(cnt)

    if not check:
        hull = cv2.convexHull(cnt)
        cv2.drawContours(img1, [hull], 0, (0, 255, 0), 3)
        cv2.imshow('convexhull', img1)

    cv2.imshow('contour', img)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


def convex2():
    img = cv2.imread('../images/star.png')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[3]

    x, y, w, h = cv2.boundingRect(cnt)
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 0, 255), 3)

    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)

    cv2.drawContours(img, [box], 0, (0, 255, 0), 3)

    cv2.imshow('rectangle', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def convex3():
    img = cv2.imread('../images/star.png')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    rows, cols = img.shape[:2]

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnt = contours[3]

    (x, y), r = cv2.minEnclosingCircle(cnt)
    center = (int(x), int(y))
    r = int(r)

    cv2.circle(img, center, r, (255, 0, 0), 3)

    ellipse = cv2.fitEllipse(cnt)
    cv2.ellipse(img, ellipse, (0, 255, 0), 3)

    [vx, vy, x, y] = cv2.fitLine(cnt, cv2.DIST_L2, 0, 0.01, 0.01)
    ly = int((-x*vy/vx) + y)
    ry = int(((cols-x)*vy/vx) + y)

    cv2.line(img, (cols-1, ry), (0, ly), (0, 0, 255), 2)

    cv2.imshow('fitting', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def convex4():
    img = cv2.imread('../images/korea_map.png')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[16]

    mmt = cv2.moments(cnt)
    cx = int(mmt['m10'] / mmt['m00'])
    cy = int(mmt['m01'] / mmt['m00'])

    x, y, w, h = cv2.boundingRect(cnt)
    korea_rect_area = w * h
    korea_area = cv2.contourArea(cnt)
    hull = cv2.convexHull(cnt)
    hull_area = cv2.contourArea(hull)
    ellipse = cv2.fitEllipse(cnt)

    aspect_ratio = w/h
    extent = korea_area / korea_rect_area
    soldity = korea_area / hull_area

    print('대한민국 Aspect Ratio:\t%.3f' %aspect_ratio)
    print('대한민국 Extent:\t%.3f' %extent)
    print('대한민국 Solidity:\t%.3f' %soldity)
    print('대한민국 Orientation:\t%.3f' %ellipse[2])

    equivalent_diameter = np.sqrt(4 * korea_area / np.pi)
    korea_radius = int(equivalent_diameter / 2)

    leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])

    cv2.circle(img, (cx, cy), 3, (0, 0, 255), -1)
    cv2.circle(img, (cx, cy), korea_radius, (0, 0, 255), 2)
    cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    cv2.ellipse(img, ellipse, (50, 50, 50), 2)

    cv2.circle(img, leftmost, 3, (0, 0, 255), -1)
    cv2.circle(img, rightmost, 3, (0, 0, 255), -1)
    cv2.circle(img, topmost, 3, (0, 0, 255), -1)
    cv2.circle(img, bottommost, 3, (0, 0, 255), -1)

    cv2.imshow('Korea Features', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def convex5():
    img = cv2.imread('../images/star2.png')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[0]

    hull = cv2.convexHull(cnt)
    cv2.drawContours(img, [hull], 0, (0, 0, 255), 2)

    hull = cv2.convexHull(cnt, returnPoints=False)
    defects = cv2.convexityDefects(cnt, hull)

    for i in range(defects.shape[0]):
        sp, ep, fp, dist = defects[i, 0]
        start = tuple(cnt[sp][0])
        end = tuple(cnt[ep][0])
        farthest = tuple(cnt[fp][0])

        cv2.circle(img, farthest, 5, (0, 255, 0), -1)

    cv2.imshow('defects', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def convex6():
    img = cv2.imread('../images/star2.png')
    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thr = cv2.threshold(imgray, 127, 255, 0)
    _, contours, _ = cv2.findContours(thr, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[0]
    cv2.drawContours(img, [cnt], 0, (0, 0, 255), 2)

    outside = (55, 70)
    inside = (240, 250)

    dist1 = cv2.pointPolygonTest(cnt, outside, True)
    dist2 = cv2.pointPolygonTest(cnt, inside, True)

    print('Contour에서 (%d, %d)까지 거리: %.3f' %(outside[0], outside[1], dist1))
    print('Contour에서 (%d, %d)까지 거리: %.3f' %(inside[0], inside[1], dist2))

    cv2.circle(img, outside, 3, (0, 255, 0), -1)
    cv2.circle(img, inside, 3, (255, 0, 255), -1)

    cv2.imshow('defects', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


convex6()